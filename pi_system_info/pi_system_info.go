// Ausgabe der System-Info für einen Raspberry Pi
//
// Thomas Wenzlaff

package main

import (
	"fmt"
	"io/ioutil"
	"log"
)

func main() {

	content, err := ioutil.ReadFile("/proc/cpuinfo")
	if err != nil {
		log.Fatal(err)
	}

 	fmt.Print(string( content))

        content_version, err := ioutil.ReadFile("/etc/debian_version")
        if err != nil {
                log.Fatal(err)
        }

        fmt.Print("Debian Version  : " + string(content_version))


        content_os, err := ioutil.ReadFile("/etc/os-release")
        if err != nil {
                log.Fatal(err)
        }

        fmt.Print(string( content_os))

	content_temp, err := ioutil.ReadFile("/sys/class/thermal/thermal_zone0/temp")
        if err != nil {
                log.Fatal(err)
        }

        fmt.Print("Temperatur  : " + string(content_temp))

}
