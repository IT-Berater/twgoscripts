// Beispiel eines REST-Servers in Go.
//
// Thomas Wenzlaff

package main

import "net/http"
import "fmt"

func main() {
  fmt.Println("Kleinhirn Server ist gestartet ...")
  http.HandleFunc("/kleinhirn", kleinhirnHandler)
  http.ListenAndServe(":8080", nil)
}

func kleinhirnHandler(w http.ResponseWriter, r *http.Request) {
  w.Write([]byte("Mindmaps von Kleinhirn.eu"))
}
