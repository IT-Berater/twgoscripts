# TWGoScripts

Create private and public Key for Ethereum blockhain. See www.wenzlaff.info.

Sample REST-Server.


## Install Go

sudo apt install golang

go version

Result:
go version go1.15.9 linux/arm

## Key Generator

git clone https://gitlab.com/IT-Berater/twgoscripts.git

cd twgoscripts

cd eth-crypt/

go mod init keygenerator.go

go run keygenerator.go

Sample Result (do not use this Keys! ):

~~~
Private Key: 0x7ee5139083a60f35a57fe760e9dff8b61a6cf69266ca04f164a695a59056bd03
Public Key: 0x04bc49bcbf314e5bdf704b1938a3072cb4d7d1a1d0f9d192ca56411feaeece1abde6623f2279c759bbe35b0b412fc76c111cd200c3d91d5e5d30b27ba42879bdff
Addresse: 0xE1c9437b046cbBfb920b63138bA7e03F9Bd63255
~~~

## Kleinhirn REST-Server

git clone https://gitlab.com/IT-Berater/twgoscripts.git

cd twgoscripts

cd rest-server

go run kleinhirn-rest-server.go

curl http://localhost:8080/kleinhirn

Result:
Mindmaps von Kleinhirn.eu

## Raspberry PI Systeminfo

git clone https://gitlab.com/IT-Berater/twgoscripts.git

cd twgoscripts

cd pi_system_info

go run pi_system_info.go

Result, auf Raspberry PI Zero:
~~~
processor	: 0
model name	: ARMv6-compatible processor rev 7 (v6l)
BogoMIPS	: 997.08
Features	: half thumb fastmult vfp edsp java tls
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xb76
CPU revision	: 7

Hardware	: BCM2835
Revision	: 9000c1
Serial		: 100000000000001
Model		: Raspberry Pi Zero W Rev 1.1
Debian Version  : 10.11
PRETTY_NAME="Raspbian GNU/Linux 10 (buster)"
NAME="Raspbian GNU/Linux"
VERSION_ID="10"
VERSION="10 (buster)"
VERSION_CODENAME=buster
ID=raspbian
ID_LIKE=debian
HOME_URL="http://www.raspbian.org/"
SUPPORT_URL="http://www.raspbian.org/RaspbianForums"
BUG_REPORT_URL="http://www.raspbian.org/RaspbianBugs"
Temperatur  : 37932
~~~




