//
// keygenerator.go erzeugt private und pulic Key mit ECDSA aus crypto Package.
//
// Thomas Wenzlaff
// 
package main

import (
    "crypto/ecdsa"
    "fmt"
    "log"

    "github.com/ethereum/go-ethereum/common/hexutil"
    "github.com/ethereum/go-ethereum/crypto"
)

func main() {
    fmt.Println("ETH Key Generator siehe www.wenzlaff.info") 

    // erzeugen eines zufälligen privaten Keys
    privateKey, err := crypto.GenerateKey() 
    if err != nil {
        log.Fatal(err)
    }
    // privaten Key mit ECDSA aus crypto Package umwandeln
    privateKeyBytes := crypto.FromECDSA(privateKey)

    // in Hexadezimal ausgeben
    fmt.Println("Private Key:", hexutil.Encode(privateKeyBytes))

    // public Key aus den privaten Key erzeugen
    publicKey := privateKey.Public()
    publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
    if !ok {
        log.Fatal("Kann nicht auf den publicKey zugreifen, er ist nicht vom type *ecdsa.PublicKey")
    }

    // umwandeln in Hexadezimal und ausgeben
    publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)
    fmt.Println("Public Key:", hexutil.Encode(publicKeyBytes))

    // Adresse erzeugen und ausgeben
    address := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
    fmt.Println("Addresse:", address)
}
